using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using UnityEditor;
using UnityEditor.SceneManagement;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace Linting
{
    public static class MissingScriptsLinter
    {
        [MenuItem("Assets/Tools/Lint missing scripts")]
        public static void LintMissingScripts()
        {
            var sceneRegex = MissingScriptsConfig.Config.scenesFilterRegex ?? "";
            sceneRegex = sceneRegex != "" ? sceneRegex : ".*"; 
            
            // Load all scenes one by one and look for missing scripts
            var allScenesGuid = AssetDatabase.FindAssets("t: scene", MissingScriptsConfig.Config.sceneFolders);
            var allScenesPath = allScenesGuid.Select(AssetDatabase.GUIDToAssetPath);
            var lintScenesPath = allScenesPath.Where(path =>
                Regex.IsMatch(Path.GetFileNameWithoutExtension(path), sceneRegex));

            var reportsList = new List<MissingScriptReport>();
            
            foreach (var scenePath in lintScenesPath)
            {
                EditorSceneManager.OpenScene(scenePath, OpenSceneMode.Single);

                var activeScene = SceneManager.GetActiveScene();
                var allSceneRoots = activeScene.GetRootGameObjects();
                var parentName = "Scene: " + activeScene.name;
                
                foreach (var rootObject in allSceneRoots)
                {
                    FindMissingScriptRecursively(rootObject, parentName, reportsList);
                }
            }

            var prefabRegex = MissingScriptsConfig.Config.prefabFilterRegex ?? "";
            prefabRegex = prefabRegex != "" ? prefabRegex : ".*"; 
            
            // Load all prefabs one by one and look for missing scripts
            var allPrefabsGuid = AssetDatabase.FindAssets("t: prefab", MissingScriptsConfig.Config.prefabFolders);
            var allPrefabsPath = allPrefabsGuid.Select(AssetDatabase.GUIDToAssetPath);
            var lintPrefabsPath = allPrefabsPath.Where(path =>
                Regex.IsMatch(Path.GetFileNameWithoutExtension(path), prefabRegex));
            
            foreach (var assetPath in lintPrefabsPath)
            {
                var prefab = PrefabUtility.LoadPrefabContents(assetPath);
                var parentName = "Prefab " + prefab.name;

                FindMissingScriptRecursively(prefab, parentName, reportsList);
            }
            
            PrintReport(reportsList);

            if (reportsList.Count > 0)
            {
                throw new Exception("Some game objects have missing scripts. Check the report above to see which.");
            }

            Debug.Log("No missing script found!");
        }

        // We could simply so do GetComponentsInChildren<Component> but we could not retrieve the name of the gameobject affected by the null 
        // The only way is to traverse all recursively
        private static void FindMissingScriptRecursively(GameObject gameObject, string parentEntity, List<MissingScriptReport> reports)
        {
            var components = gameObject.GetComponents<Component>();
            var nbMissing = components.Count(comp => comp == null);
            
            if (nbMissing > 0)
            {
                reports.Add(new MissingScriptReport{parentEntity = parentEntity, entityFullHierarchy = gameObject.FullName(), nbMissingScripts = nbMissing});
            }

            for (var i = 0; i < gameObject.transform.childCount; i++)
            {
                var child = gameObject.transform.GetChild(i);
                FindMissingScriptRecursively(child.gameObject, parentEntity, reports);
            }
        }

        private static string FullName(this GameObject gameObject)
        {
            var names = new List<string>();
            var transform = gameObject.transform;
            while (transform != null)
            {
                names.Insert(0, transform.name);
                transform = transform.parent;
            }

            return string.Join("/", names);
        }
        
        private static void PrintReport(List<MissingScriptReport> reports)
        {
            var sb = new StringBuilder();
            foreach (var report in reports)
            {
                sb.AppendLine(report.ToString());
            }

            Debug.Log(sb);
        }

        private class MissingScriptReport
        {
            public string parentEntity;
            public string entityFullHierarchy;
            public int nbMissingScripts;

            public override string ToString()
            {
                return
                    $"In {parentEntity}: {entityFullHierarchy} has {nbMissingScripts} missing script{(nbMissingScripts > 1 ? "s" : "")}";
            }
        }
    }
}