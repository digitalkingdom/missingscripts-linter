using UnityEditor;
using UnityEngine;

namespace Linting
{
    public class MissingScriptsConfig : ScriptableObject
    {
        [Tooltip("Pattern of linted scene files. Files that do not match will be omitted. Leave empty to lint all")]
        public string scenesFilterRegex;
        
        [Tooltip("List of folders containing scenes to lint. Relative to the project root directory.")]
        public string[] sceneFolders =
        {
            "Assets/Scenes"
        };
        
        [Tooltip("Pattern of linted prefab files. Files that do not match will be omitted. Leave empty to lint all")]
        public string prefabFilterRegex;
        
        [Tooltip("List of folders containing prefabs to lint. Relative to the project root directory.")]
        public string[] prefabFolders =
        {
            "Assets/Prefabs"
        };

        public static MissingScriptsConfig Config
        {
            get
            {
                var instance = Resources.Load<MissingScriptsConfig>("MissingScriptsConfig");
                if (instance == null)
                {
                    instance = CreateInstance<MissingScriptsConfig>();
                    AssetDatabase.CreateAsset (instance, $"Assets/Resources/MissingScriptsConfig.asset");
                }

                return instance;
            }
        }
    }
}