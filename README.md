# Missing scripts Linter

Installs under `Assets/Tools/Lint missing scripts`.
This will open all scenes and prefabs under the Asset folder and look for all MonoBehaviour scripts. Any object with a missing script will be reported by unity and logged

You can use this in the CI by running the following command line:

```bash
UNITY_EXECUTABLE -batchmode -quit -nographics -executemethod Linting.MissingScriptsLinter.LintMissingScripts
```

A log will be printed listing all the linted assets, and their state (wrong/ok).
An exception will be raised if an object with a missing script was found.

